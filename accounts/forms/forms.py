
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django import forms
from accounts.models import Story

class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'username',
            'password1',
            'password2'
        ]

class CreateStoryForm(forms.ModelForm):
    story = forms.CharField(label='',
                            required=True,
                            widget=forms.Textarea(
                                attrs={
                                    'placeholder': 'Enter your story here',
                                    'name': 'story',
                                    'id': 'story',
                                    'cols': 150,
                                    'rows': 30
                                              }))

    class Meta:
        model = Story
        fields = ['story']
