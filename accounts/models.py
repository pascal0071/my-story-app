from django.db import models
from datetime import datetime
from django.contrib.auth.models import User
from django.urls import reverse

# Create your models here.

class Story(models.Model):
    user = models.ForeignKey(User, default=None, on_delete=models.CASCADE)
    story = models.TextField(null=False)
    submission_date = models.DateTimeField(default=datetime.utcnow)

