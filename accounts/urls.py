from django.urls import path
from accounts.views import HomeView, LoginView, RegisterView, LogoutView, CreateStoryView, YourStoryView, OtherStoryView


urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('login/', LoginView.as_view(), name='login'),
    path('register/', RegisterView.as_view(), name='register'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('story/', CreateStoryView.as_view(), name='write-story'),
    path('your-story/', YourStoryView.as_view(), name='your-story'),
    path('other-story/', OtherStoryView.as_view(), name='other-story'),
]