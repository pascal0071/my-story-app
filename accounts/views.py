from django.shortcuts import render, redirect
from accounts.models import Story
from accounts.forms.forms import CreateUserForm, CreateStoryForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.views.generic import TemplateView, CreateView, ListView, View
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy




# Create your views here.

class HomeView(LoginRequiredMixin, TemplateView):
    login_url = 'login'
    redirect_field_name = 'redirect_to'
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user'] = User.objects.get(username=self.request.user.username)
        return context


class LoginView(View):

    def post(self, request):
        if request.user.is_authenticated:
            return redirect('home')
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('home')
            else:
                messages.info(request, 'Username/Password incorrect')

    def get(self, request):
        if request.user.is_authenticated:
            return redirect('home')

        return render(request, 'loginview.html', {})


class RegisterView(SuccessMessageMixin, CreateView):
    form_class = CreateUserForm
    success_url = reverse_lazy('login')
    template_name = 'registerview.html'
    success_message = 'User Created successfully'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('home')
        return super(RegisterView, self).get(request, *args, **kwargs)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect('login')


class CreateStoryView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    login_url = 'login'
    redirect_field_name = 'redirect_to'
    model = Story

    template_name = 'write-story.html'
    form_class = CreateStoryForm
    success_message = 'Your story has been saved'
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class YourStoryView(LoginRequiredMixin, ListView):
    template_name = 'your-story.html'
    login_url = 'login'
    redirect_field_name = 'redirect_to'
    model = Story
    context_object_name = 'stories'

    def get_queryset(self):
        queryset = Story.objects.filter(user=self.request.user)
        return queryset


class OtherStoryView(LoginRequiredMixin, ListView):
    template_name = 'other-story.html'
    login_url = 'login'
    redirect_field_name = 'redirect_to'
    model = Story
    context_object_name = 'stories'

    def get_queryset(self):
        queryset = Story.objects.exclude(user=self.request.user)
        return queryset
