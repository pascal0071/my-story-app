from rest_framework import generics
from story_api.serializers import CreateStorySerializer, LoginSerializer, RegisterSerializer, FetchStorySerializer
from accounts.models import Story
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth import login, logout
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from django.http import Http404

# Create your views here.

class AllStoryAPI(generics.ListAPIView):
    permission_classes = (IsAuthenticated, )
    try:
        queryset = Story.objects.all()
    except Story.DoesNotExist:
        raise Http404

    serializer_class = FetchStorySerializer

class YourStoryAPI(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = FetchStorySerializer

    def get(self, request):
        try:
            queryset = Story.objects.get(user=request.user)
        except Story.DoesNotExist:
            raise Http404('You have no story')
        your_story_serializer = FetchStorySerializer(queryset)
        return Response(your_story_serializer.data)

class OtherStoryAPI(generics.ListAPIView):
    permission_classes = [IsAuthenticated, ]
    serializer_class = FetchStorySerializer

    def get(self, request):
        try:
            queryset = Story.objects.exclude(user=request.user)
        except :
            raise Http404("No other story")

        serializer = FetchStorySerializer(queryset)
        return Response(serializer.data)





"""class LoginAPI(APIView):
    permission_classes = (AllowAny, )
    def post(self, request):
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)

        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key}, status=200)"""

class LogoutAPI(APIView):
    authentication_classes = (TokenAuthentication, )


    def post(self, request):
        logout(request)
        return Response(status=status.HTTP_204_NO_CONTENT)








class RegisterUserAPI(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        reg_serializer = RegisterSerializer(data=request.data)
        if reg_serializer.is_valid():
            new_user = reg_serializer.save()
            if new_user:
                return Response(status=status.HTTP_201_CREATED)
        return Response(reg_serializer.errors, status=status.HTTP_400_BAD_REQUEST)




class CreateStoryAPI(APIView):
    permission_classes = [IsAuthenticated, ]
    serializer_class = CreateStorySerializer

    def post(self, request):
        serializer = CreateStorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

