"""story_app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.urls import path, include
from .views import YourStoryAPI, OtherStoryAPI, CreateStoryAPI, LogoutAPI, AllStoryAPI, RegisterUserAPI
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

app_name = 'story_api'


urlpatterns = [
    path('', AllStoryAPI.as_view(), name='all-story-api'),
    path('your-story/', YourStoryAPI.as_view(), name='your-story-api'),
    path('other-story/', OtherStoryAPI.as_view(), name='other-story-api'),
    path('create-story', CreateStoryAPI.as_view(), name='create-story-api'),
    #path('login/', LoginAPI.as_view(), name='login'),
    path('logout/', LogoutAPI.as_view(), name='logout'),
    path('register/', RegisterUserAPI.as_view(), name='register'),
    path('login/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    #path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),



]
