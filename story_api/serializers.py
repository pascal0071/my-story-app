from rest_framework import serializers
from accounts.models import Story
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from rest_framework import exceptions



class CreateStorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Story
        fields = ['user', 'story']

class FetchStorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Story
        fields = ['user', 'story', 'submission_date']

class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        username = data.get('username', '')
        password = data.get('password', '')

        if username and password:
            user = authenticate(username=username, password=password)
            if user:
                if user.is_active:
                    data['user'] = user
                else:
                    msg = 'user is disabled'
                    raise exceptions.ValidationError(msg)
            else:
                msg = 'unable to login with given credentials'
                raise exceptions.ValidationError(msg)
        else:
            msg = 'must provide both username and password'
            raise exceptions.ValidationError(msg)
        return data

class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'password', 'first_name', 'last_name')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = User(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance